# WorldQuant University MSc Fin. Engineering Capstone Project 
## Group 22 C18-S3


This is the repository thats hosts our iPython Notebook for much of the computational analysis for our Project on " Smart Derivatives Contracting: Automating Interest Rates Swaps in the OTC Market through the DAML".

We have provided a requirements.txt file to enable duplication of the environment necessary to run the notebook.

The iPython notebook was developed on colaboratory.

You will need Internet access to run the notebook as it fetches live data from the FRED database using the pandas datareader.
The data sources are run in memory. The option to save them as .csv has been commented out so the folder structure does not interfare with the execution of the script. 
You can uncomment save section and input appropriate folder path.

The platform for implementing the Smart Contract can be cloned from : https://github.com/digital-asset/ex-cdm-swaps

Our implementation of the DAML Smart Contract Plaform was instantiated with Google Cloud Platform Virtual Machine Instance running Centos 8 and could be available at http://35.231.43.246:4000/. 

Though it may not be continuously accessible as a result of Google Cloud Platform Compute Engine costs.

Thank you.

